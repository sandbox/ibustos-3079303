<?php

namespace Drupal\new_user_action\Plugin\Action;

use Drupal\Component\Utility\Random;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a new user.
 *
 * @Action(
 *   id = "new_user_action",
 *   label = @Translation("Create a new Drupal user"),
 *   type = "user"
 * )
 */
class NewUser extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  const PASSWORD_DEFAULT_METHOD = 'initial';

  const PASSWORD_RESET_METHOD = 'reset';

  const PASSWORD_DEFAULT = '123456';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'notify' => FALSE,
      'password' => self::PASSWORD_DEFAULT_METHOD,
      'username' => 'drupaluser',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['notify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify User of new account.'),
      '#description' => $this->t('only applies if an email address has been set programmatically.'),
      '#default_value' => $this->configuration['notify'],
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User name prefix'),
      '#description' => $this->t('The user name prefix. It will prepend a 7 letter random string. The entire username can be set programmatically via the provided hook.'),
      '#default_value' => $this->configuration['username'],
    ];
    $form['password'] = [
      '#type' => 'radios',
      '#title' => $this->t('Password assignation method'),
      '#description' => $this->t('Select how a password should be assigned to the new account.'),
      '#options' => [
        self::PASSWORD_DEFAULT_METHOD => $this->t('If no password has been set programmatically, assign the following temporary password: "@pass". (Default)', [
          '@pass' => self::PASSWORD_DEFAULT,
        ]),
        self::PASSWORD_RESET_METHOD => $this->t('Send the default Password Recovery email so the user can set his/her password (only applies if an email address has been set programmatically, otherwise it will use the default method).'),
      ],
      '#default_value' => $this->configuration['password'],
    ];
    $form['timezone'] = [
      '#type' => 'select',
      '#title' => $this->t('Time zone'),
      '#options' => system_time_zones(TRUE, TRUE),
      '#description' => $this->t('Select the desired local time and time zone. Dates and times throughout this site will be displayed using this time zone. (Applies only if no timezone has been applied programmatically).'),
    ];
    if (isset($this->configuration['timezone'])) {
      $form['timezone']['#default_value'] = $this->configuration['timezone'];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['notify'] = $form_state->getValue('notify');
    $this->configuration['username'] = $form_state->getValue('username');
    $this->configuration['password'] = $form_state->getValue('password');
    $this->configuration['timezone'] = $form_state->getValue('timezone');
  }

  /**
   * {@inheritdoc}
   */
  public function execute($account = NULL) {
    $new = $this->entityTypeManager->getStorage('user')->create();

    // Allow other modules to alter the new entity.
    $this->moduleHandler->alter('new_user_action_object', $new, $this, $account);

    // Set default user name.
    if (!$new->getAccountName()) {
      $random = new Random();
      $new->setUsername($this->configuration['username'] . $random->name());
    }

    // Set default password.
    if (!$new->getPassword()) {
      $new->setPassword(self::PASSWORD_DEFAULT);
    }

    // Set the timezone.
    if (!$new->getTimeZone() && isset($this->configuration['timezone'])) {
      $new->set('timezone', $this->configuration['timezone']);
    }

    // Save the user.
    $new->save();

    // We can notify user.
    if ($new->getEmail()) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();

      if ($this->configuration['password'] == self::PASSWORD_RESET_METHOD) {
        // Send reset password email.
        _user_mail_notify('password_reset', $new, $langcode);
      }
      if ($this->configuration['notify']) {
        // Notify user of account creation.
        _user_mail_notify('register_admin_created', $new, $langcode);
      }
    }
    return $new;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object = NULL, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access = AccessResult::allowedIfHasPermission($account, 'administer users');
    return $return_as_object ? $access : $access->isAllowed();
  }

}
