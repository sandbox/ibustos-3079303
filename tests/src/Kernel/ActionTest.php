<?php

namespace Drupal\Tests\new_user_action\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Verifies and tests the new user action.
 *
 * @group new_user_action
 */
class ActionTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'action',
    'new_user_action',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('system', ['sequences']);
    // @see https://www.drupal.org/project/drupal/issues/540008.
    $this->createUser();
  }

  /**
   * Validates action usage by a privileged user.
   *
   * @dataProvider dataProvider
   */
  public function testAction(array $permissions, $expected) {
    $user = $this->createUser($permissions);

    // At this point there must be one and only one user.
    $users = $this->container->get('entity_type.manager')->getStorage('user')->loadMultiple();
    $this->assertEqual(count($users), 2);

    // Create the action.
    $plugin_manager = $this->container->get('plugin.manager.action');
    $configuration = [];
    $action = $plugin_manager->createInstance('new_user_action', $configuration);

    // Execute it.
    if ($action->access(NULL, $user)) {
      $action->execute();
    }

    // At this point there must be two users.
    $users = $this->container->get('entity_type.manager')->getStorage('user')->loadMultiple();
    $this->assertEqual(count($users), $expected);
  }

  /**
   * Provides data for testAction.
   */
  public function dataProvider() {
    $data = [];

    $data[] = [
      ['administer users'],
      3,
    ];

    $data[] = [
      [],
      2,
    ];

    return $data;
  }

}
