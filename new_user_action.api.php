<?php

/**
 * @file
 * Hooks for the new_user_action module.
 */

use Drupal\Core\Action\ActionInterface;
use Drupal\user\UserInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows to alter the user entity being created by new_user_action.
 *
 * @param \Drupal\user\UserInterface $account
 *   The account being built.
 * @param \Drupal\Core\Action\ActionInterface $action
 *   The action plugin instance invoking this hook.
 * @param \Drupal\user\UserInterface $trigger_account
 *   (optional) The account that triggered the action, if any.
 */
function hook_new_user_action_object_alter(UserInterface $account, ActionInterface $action, UserInterface $trigger_account = NULL) {
  $account->setEmail('test@test.com');
}

/**
 * @} End of "addtogroup hooks".
 */
